<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['prefix' => 'base', 'as' => 'base.'], function () {
    Route::get('/wilayah', 'BaseController@wilayah')->name("wilayah");
    Route::get('/type', 'BaseController@type')->name("type");
    Route::get('/menu', 'BaseController@menu')->name("menu");
});

Route::name('user.')->group(function(){
    Route::group(['middleware' => ['auth', 'verified']], function () {

        Route::group(['prefix' => 'dashboard', 'as'=>'dashboard.'], function () {
            Route::get('/','DashboardController@index')->name('index');
            Route::get('/counter', 'DashboardController@counter')->name('counter');
        });
        // Route::get('/dashboard', function () {
        //     return Inertia::render('Dashboard');
        // })->name('dashboard');

        Route::group(['prefix' => 'purchase', 'as'=>'purchase.'], function () {
            Route::get('/', 'PurchaseController@index')->name('index');
            Route::get('/create', 'PurchaseController@create')->name('create');
            Route::post('/store','PurchaseController@store')->name('store');
            Route::get('/edit/{id}','PurchaseController@edit')->name('edit');
            Route::post('/update/{id}','PurchaseController@update')->name('update');
            Route::get('/show/{id}', 'PurchaseController@show')->name('show');
            Route::delete('/delete/{id}','PurchaseController@destroy')->name('delete');
            Route::get('/data', 'PurchaseController@data')->name('data');
            Route::post('/state/{id}', 'PurchaseController@state')->name('state');
            Route::post('/Cash','PurchaseController@payment')->name('payment');

            Route::group(['prefix' => 'supplier', 'as'=>'supplier.'], function () {
                Route::get('/', 'SupplierController@index')->name('index');
                Route::get('/create', 'SupplierController@create')->name('create');
                Route::post('/store','SupplierController@store')->name('store');
                Route::get('/edit/{id}','SupplierController@edit')->name('edit');
                Route::post('/update/{id}','SupplierController@update')->name('update');
                Route::get('/detail/{id}', 'SupplierController@show')->name('show');
                Route::delete('/delete/{id}','SupplierController@destroy')->name('delete');
                Route::get('/data', 'SupplierController@data')->name('data');
            });

            Route::group(['prefix' => 'investor', 'as'=>'investor.'], function () {
                Route::get('/', 'InvestorController@index')->name('index');
                Route::get('/create', 'InvestorController@create')->name('create');
                Route::post('/store','InvestorController@store')->name('store');
                Route::get('/edit/{id}','InvestorController@edit')->name('edit');
                Route::post('/update/{id}','InvestorController@update')->name('update');
                Route::get('/detail/{id}', 'InvestorController@show')->name('show');
                Route::delete('/delete/{id}','InvestorController@destroy')->name('delete');
                Route::get('/data', 'InvestorController@data')->name('data');
            });

        });

        
        Route::group(['prefix' => 'sale', 'as'=>'sale.'], function () {
            Route::get('/', 'SaleController@index')->name('index');
            Route::get('/create', 'SaleController@create')->name('create');
            Route::post('/store','SaleController@store')->name('store');
            Route::get('/edit/{id}','SaleController@edit')->name('edit');
            Route::post('/update/{id}','SaleController@update')->name('update');
            Route::get('/show/{id}', 'SaleController@show')->name('show');
            Route::delete('/delete/{id}','SaleController@destroy')->name('delete');
            Route::get('/data', 'SaleController@data')->name('data');
            Route::post('/state/{id}', 'SaleController@state')->name('state');

            Route::group(['prefix' => 'customer', 'as'=>'customer.'], function () {
                Route::get('/', 'CustomerController@index')->name('index');
                Route::get('/create', 'CustomerController@create')->name('create');
                Route::post('/store','CustomerController@store')->name('store');
                Route::get('/edit/{id}','CustomerController@edit')->name('edit');
                Route::post('/update/{id}','CustomerController@update')->name('update');
                Route::get('/detail/{id}', 'CustomerController@show')->name('show');
                Route::delete('/delete/{id}','CustomerController@destroy')->name('delete');
                Route::get('/data', 'CustomerController@data')->name('data');
            });
        });
        
        Route::group(['prefix' => 'inventory', 'as'=>'inventory.'], function () {

            Route::get('/create', 'StockMoveController@create')->name('create');
            Route::post('/store','StockMoveController@store')->name('store');
            Route::get('/edit/{id}','StockMoveController@edit')->name('edit');
            Route::delete('/delete/{id}','StockMoveController@destroy')->name('delete');
            Route::get('/data', 'StockMoveController@data')->name('data');

            Route::group(['prefix' => 'location', 'as'=>'location.'], function () {
                Route::get('/', 'LocationController@index')->name('index');
                Route::get('/create', 'LocationController@create')->name('create');
                Route::post('/store','LocationController@store')->name('store');
                Route::get('/edit/{id}','LocationController@edit')->name('edit');
                Route::post('/update/{id}','LocationController@update')->name('update');
                Route::get('/detail/{id}', 'LocationController@show')->name('show');
                Route::delete('/delete/{id}','LocationController@destroy')->name('delete');
                Route::get('/data', 'LocationController@data')->name('data');
                Route::get('/pent', 'LocationController@pent')->name('pent');
            });

            Route::group(['prefix' => 'cattle', 'as'=>'cattle.'], function () {
                Route::get('/', 'CattleController@index')->name('index');
                Route::get('/create', 'CattleController@create')->name('create');
                Route::post('/store','CattleController@store')->name('store');
                Route::get('/edit/{id}','CattleController@edit')->name('edit');
                Route::post('/update/{id}','CattleController@update')->name('update');
                Route::get('/detail/{id}', 'CattleController@show')->name('show');
                Route::delete('/delete/{id}','CattleController@destroy')->name('delete');
                Route::get('/data', 'CattleController@data')->name('data');
            });

            Route::group(['prefix' => 'vehicle', 'as'=>'vehicle.'], function () {
                Route::get('/', 'VehicleController@index')->name('index');
                Route::get('/create', 'VehicleController@create')->name('create');
                Route::post('/store','VehicleController@store')->name('store');
                Route::get('/edit/{id}','VehicleController@edit')->name('edit');
                Route::post('/update/{id}','VehicleController@update')->name('update');
                Route::get('/detail/{id}', 'VehicleController@show')->name('show');
                Route::delete('/delete/{id}','VehicleController@destroy')->name('delete');
                Route::get('/data', 'VehicleController@data')->name('data');
            });
            
            Route::get('/{state}', 'StockMoveController@index')->name('index');
            Route::get('/{state}/{id}', 'StockMoveController@show')->name('show');
            Route::post('/{state}/{id}/update','StockMoveController@update')->name('update');
            
        });

        Route::group(['prefix' => 'medical', 'as'=>'medical.'], function () {
            Route::get('/index', 'MedicalCheckController@index')->name('index');
            Route::get('/create', 'MedicalCheckController@create')->name('create');
            Route::post('/store','MedicalCheckController@store')->name('store');
            Route::get('/edit/{id}','MedicalCheckController@edit')->name('edit');
            Route::post('/update/{id}','MedicalCheckController@update')->name('update');
            Route::get('/detail/{id}', 'MedicalCheckController@show')->name('show');
            Route::delete('/delete/{id}','MedicalCheckController@destroy')->name('delete');
            Route::get('/data', 'MedicalCheckController@data')->name('data');

        });

        Route::group(['prefix' => 'settings', 'as'=>'settings.'], function () {
            Route::get('/', 'SettingController@index')->name('index');
            Route::get('/profile', 'SettingController@profile')->name('profile');
            Route::get('/password', 'SettingController@password')->name('password');

        });

        Route::group(['prefix' => 'finance', 'as'=>'finance.'], function () {

            Route::group(['prefix' => 'payment', 'as'=>'payment.'], function () {
                Route::get('/', 'PaymentController@index')->name('index');
                Route::get('/create', 'PaymentController@create')->name('create');
                Route::post('/store','PaymentController@store')->name('store');
                Route::get('/edit/{id}','PaymentController@edit')->name('edit');
                Route::post('/update/{id}','PaymentController@update')->name('update');
                Route::get('/detail/{id}', 'PaymentController@show')->name('show');
                Route::delete('/delete/{id}','PaymentController@destroy')->name('delete');
                Route::get('/data', 'PaymentController@data')->name('data');
                Route::get('/pent', 'PaymentController@pent')->name('pent');
            });

            Route::group(['prefix' => 'payment-method', 'as'=>'payment_method.'], function () {
                Route::get('/', 'PaymentMethodController@index')->name('index');
                Route::get('/create', 'PaymentMethodController@create')->name('create');
                Route::post('/store','PaymentMethodController@store')->name('store');
                Route::get('/edit/{id}','PaymentMethodController@edit')->name('edit');
                Route::post('/update/{id}','PaymentMethodController@update')->name('update');
                Route::get('/detail/{id}', 'PaymentMethodController@show')->name('show');
                Route::delete('/delete/{id}','PaymentMethodController@destroy')->name('delete');
                Route::get('/data', 'PaymentMethodController@data')->name('data');
                Route::get('/pent', 'PaymentMethodController@pent')->name('pent');
            });

            Route::group(['prefix' => 'cash', 'as'=>'cash.'], function () {
                Route::get('/', 'CashController@index')->name('index');
                Route::get('/create', 'CashController@create')->name('create');
                Route::post('/store','CashController@store')->name('store');
                Route::get('/edit/{id}','CashController@edit')->name('edit');
                Route::post('/update/{id}','CashController@update')->name('update');
                Route::get('/detail/{id}', 'CashController@show')->name('show');
                Route::delete('/delete/{id}','CashController@destroy')->name('delete');
                Route::get('/data', 'CashController@data')->name('data');
            });

            
            Route::group(['prefix' => 'investasi', 'as'=>'invest.'], function () {
                Route::get('/', 'InvestController@index')->name('index');
                Route::get('/create', 'InvestController@create')->name('create');
                Route::post('/store','InvestController@store')->name('store');
                Route::get('/edit/{id}','InvestController@edit')->name('edit');
                Route::post('/update/{id}','InvestController@update')->name('update');
                Route::get('/detail/{id}', 'InvestController@show')->name('show');
                Route::delete('/delete/{id}','InvestController@destroy')->name('delete');
                Route::get('/data', 'InvestController@data')->name('data');
            });

            
        });

    });

});

require __DIR__.'/auth.php';

