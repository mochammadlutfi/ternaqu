<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\Location;
use App\Models\LocationPent;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Inertia::render('Location/Index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Location/Form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'code' => 'required',
            'name' => 'required',
            'address' => 'required',
            'prov_id' => 'required',
            'kota_id' => 'required',
            'kec_id' => 'required',
            'kel_id' => 'required',
        ];

        $pesan = [
            'code.required' => 'Kode Lokasi Wajib Diisi!',
            'name.required' => 'Nama Lokasi Wajib Diisi!',
            'address.required' => 'Alamat Wajib Diisi!',
            'prov_id.required' => 'Provinsi Wajib Diisi!',
            'kota_id.required' => 'Kota/Kabupaten Wajib Diisi!',
            'kec_id.required' => 'Kecamatan Wajib Diisi!',
            'kel_id.required' => 'Kelurahan/Desa Wajib Diisi!',
        ];

        $validator = Validator::make($request->all(), $rules, $pesan);
        if ($validator->fails()){
            return back()->withErrors($validator->errors());
        }else{
            DB::beginTransaction();
            try{
                    $data = new Location();
                    $data->name = $request->name;
                    $data->address = $request->address;
                    $data->prov_id = $request->prov_id;
                    $data->kota_id = $request->kota_id;
                    $data->kec_id = $request->kec_id;
                    $data->kel_id = $request->kel_id;
                    $data->code = $request->code;
                    $data->company_id = auth()->user()->company_id;
                    $data->save();

                    foreach($request->lines as $i){
                        $line = new LocationPent();
                        $line->location_id = $data->id;
                        $line->code = $i['code'];
                        $line->name = $i['name'];
                        $line->capacity = $i['capacity'];
                        $line->company_id = auth()->user()->company_id;
                        $data->line()->save($line);
                    }

            }catch(\QueryException $e){
                DB::rollback();
                return back();
            }
            DB::commit();
            return redirect()->route('user.inventory.location.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        return Inertia::render('Inventory/Location/Show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Location::with('lines')->where('id', $id)->first();

        return Inertia::render('Location/Form',[
            'editMode' => true,
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $rules = [
            'code' => 'required',
            'name' => 'required',
            'address' => 'required',
            'prov_id' => 'required',
            'kota_id' => 'required',
            'kec_id' => 'required',
            'kel_id' => 'required',
        ];

        $pesan = [
            'code.required' => 'Kode Lokasi Wajib Diisi!',
            'name.required' => 'Nama Lokasi Wajib Diisi!',
            'address.required' => 'Alamat Wajib Diisi!',
            'prov_id.required' => 'Provinsi Wajib Diisi!',
            'kota_id.required' => 'Kota/Kabupaten Wajib Diisi!',
            'kec_id.required' => 'Kecamatan Wajib Diisi!',
            'kel_id.required' => 'Kelurahan/Desa Wajib Diisi!',
        ];

        $validator = Validator::make($request->all(), $rules, $pesan);
        if ($validator->fails()){
            return back()->withErrors($validator->errors());
        }else{
            DB::beginTransaction();
            try{
                    $data = Location::find($id);
                    $data->name = $request->name;
                    $data->address = $request->address;
                    $data->prov_id = $request->prov_id;
                    $data->kota_id = $request->kota_id;
                    $data->kec_id = $request->kec_id;
                    $data->kel_id = $request->kel_id;
                    $data->code = $request->code;
                    $data->save();

                    foreach($request->lines as $i){
                    
                        if(array_key_exists("id", $i)){
                            $line_id = $i['id'];
                        }else{
                            $line_id = null;
                        }
    
                        $line = LocationPent::firstOrNew(['id' =>  $line_id]);
                        $line->code = $i['code'];
                        $line->name = $i['name'];
                        $line->capacity = $i['capacity'];
                        $line->company_id = auth()->user()->company_id;
                        $data->lines()->save($line);
                    }
    
                    $remove = LocationPent::where('location_id', $data->id)->whereIn('id', $request->removedLines)->delete();

            }catch(\QueryException $e){
                DB::rollback();
                return back();
            }
            DB::commit();
            return redirect()->route('user.inventory.location.show', $data->id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try{
            $hapus_db = Location::destroy($id);
        }catch(\QueryException $e){
            DB::rollback();
            return back();
        }

        DB::commit();
        return redirect()->route('user.inventory.location.index');
    }
    
    public function data(Request $request)
    {
        $sort = !empty($request->sort) ? $request->sort : 'id';
        $sortDir = !empty($request->sortDir) ? $request->sortDir : 'desc';
        $limit = ($request->limit) ? $request->limit : 25;

        $relawan_id = $request->relawan_id;
        $company_id = auth()->user()->company_id;

        // dd($relawan_id);

        $query = DB::table("location as p")
        ->join("res_provinsi as pr", function($join){
            $join->on("pr.id", "=", "p.prov_id");
        })
        ->join("res_kota as kt", function($join){
            $join->on("kt.id", "=", "p.kota_id");
        })
        ->join("res_kecamatan as kec", function($join){
            $join->on("kec.id", "=", "p.kec_id");
        })
        ->join("res_kelurahan as kel", function($join){
            $join->on("kel.id", "=", "p.kel_id");
        })
        ->select("p.id", "p.name", "p.code", "p.address", "pr.nama as prov", "kt.nama as kota", "kec.nama as kec", "kel.nama as kel", 
        DB::Raw("(SELECT SUM(capacity) FROM location_pents WHERE location_id=p.id) as capacity"),
        DB::Raw("(SELECT COUNT(*) FROM res_cattle WHERE location_id=p.id) as count_ternak"))
        ->where("p.company_id", "=", $company_id);
        
        if($request->page){
            $data = $query->paginate($limit);
        }else{
            $data = $query->get();
        }
        
        return response()->json($data);
    }
    
    public function pent(Request $request)
    {
        $sort = !empty($request->sort) ? $request->sort : 'id';
        $sortDir = !empty($request->sortDir) ? $request->sortDir : 'desc';
        $limit = ($request->limit) ? $request->limit : 25;

        $location_id = $request->location_id;
        $company_id = auth()->user()->company_id;

        // dd($relawan_id);

        $query = DB::table("location_pents as p")
        ->select("p.*")
        ->where("p.location_id", "=", $location_id)
        ->where("p.company_id", "=", $company_id);
        
        if($request->page){
            $data = $query->paginate($limit);
        }else{
            $data = $query->get();
        }
        
        return response()->json($data);
    }
}
