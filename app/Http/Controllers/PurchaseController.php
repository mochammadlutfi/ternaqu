<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use App\Helpers\SquenceHelper;
use App\Models\Purchase;
use App\Models\PurchaseLine;
use App\Models\StockMove;
use App\Models\StockMoveLine;
use App\Models\Payment;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Inertia::render('Purchase/Index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Purchase/Form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $rules = [
            'supplier_id' => 'required',
            'date' => 'required',
            'location_id' => 'required',
            'due_date' => 'required',
            'schedule_date' => 'required',
            'lines.*.type_id' => 'required',
        ];

        $pesan = [
            'supplier_id.required' => 'Supplier Wajib Diisi!',
            'date.required' => 'Tanggal Wajib Diisi!',
            'location_id.required' => 'Lokasi Penerimaan Wajib Diisi!',
            'schedule_date.required' => 'Tanggal Penerimaan Wajib Diisi!',
            'due_date.required' => 'Jatuh Tempo Wajib Diisi!',
        ];

        $validator = Validator::make($request->all(), $rules, $pesan);
        if ($validator->fails()){
            return back()->withErrors($validator->errors());
        }else{
            DB::beginTransaction();
            try{
                $company_id = auth()->user()->company_id;
                $data = new Purchase();
                $data->name = SquenceHelper::seq_purchase($company_id);
                $data->supplier_id = $request->supplier_id;
                $data->order_date = Carbon::parse($request->date);
                $data->due_date = Carbon::parse($request->due_date);
                $data->schedule_date = Carbon::parse($request->schedule_date);
                $data->location_id = $request->location_id;
                $data->ref = $request->ref;
                $data->total = $request->total;
                $data->discount_type = $request->discount_type;
                $data->discount_value = $request->discount_value;
                $data->grand_total = $request->grand_total;
                $data->residual = $request->grand_total;
                $data->company_id = $company_id;
                $data->state = 'draft';
                $data->created_uid = auth()->user()->id;
                $data->save();

                
                foreach($request->lines as $i){
                    $line = new PurchaseLine();
                    $line->investor_id = $i['investor_id'];
                    $line->percentage = $i['persentase'];
                    $line->mou_date = Carbon::parse($i['mou_date']);
                    $line->dob = Carbon::parse($i['dob']);
                    $line->jk = $i['jk'];
                    $line->type_id = $i['type_id'];
                    $line->weight = $i['weight'];
                    $line->price = $i['price'];
                    $line->company_id = $company_id;
                    $data->line()->save($line);
                }

            }catch(\QueryException $e){
                DB::rollback();
                return back();
            }
            DB::commit();
            return redirect()->route('user.purchase.show', $data->id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $company_id = auth()->user()->company_id;

        $data = Purchase::with('payment')
        ->join("res_partner as pr", function($join){
            $join->on("pr.id", "=", "purchase.supplier_id");
        })
        ->leftjoin("location as l", function($join){
            $join->on("l.id", "=", "purchase.location_id");
        })
        ->select("purchase.*", "l.name as location", "pr.name as supplier", 
            DB::Raw("(SELECT COUNT(*) FROM stock_move WHERE type='in' AND ordertable_id=purchase.id) as penerimaan"),
            DB::Raw("(SELECT COUNT(*) FROM payments WHERE type='outcome' AND paymenttable_id=purchase.id) as pembayaran")
        )
        ->where("purchase.company_id", "=", $company_id)
        ->where('purchase.id', $id)->first();

        $data->lines = PurchaseLine::join("res_type as rt", function($join){
            $join->on("rt.id", "=", "purchase_line.type_id");
        })
        ->leftjoin('res_partner as rp', 'rp.id','=', 'purchase_line.investor_id')
        ->select("purchase_line.*", "rt.name as type", "rp.name as investor")
        ->where('purchase_line.order_id', $data->id)->get();

        if(in_array($data->state, ['confirm', 'done'])){
            $data->move_id = StockMove::where('ordertable_type', 'App\Models\Purchase')->where('ordertable_id', $data->id)->first()->id;
        }

        return Inertia::render('Purchase/Show', [
            'data' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company_id = auth()->user()->company_id;

        $data = Purchase::join("res_partner as pr", function($join){
            $join->on("pr.id", "=", "purchase.supplier_id");
        })
        ->select("purchase.*", "pr.name as supplier")
        ->where("purchase.company_id", "=", $company_id)
        ->where('purchase.id', $id)->first();

        $data->lines = PurchaseLine::join("res_type as rt","rt.id", "=", "purchase_line.type_id")
        ->leftjoin('res_partner as rp', 'rp.id','=', 'purchase_line.investor_id')
        ->select("purchase_line.*", "rt.name as type", "rp.name as investor")
        ->where('purchase_line.order_id', $data->id)->get();

        return Inertia::render('Purchase/Form',[
            'editMode' => true,
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'supplier_id' => 'required',
            'date' => 'required',
        ];

        $pesan = [
            'supplier_id.required' => 'Supplier Wajib Diisi!',
            'date.required' => 'Tanggal Wajib Diisi!',
        ];

        $validator = Validator::make($request->all(), $rules, $pesan);
        if ($validator->fails()){
            return back()->withErrors($validator->errors());
        }else{
            DB::beginTransaction();
            try{
                $company_id = auth()->user()->company_id;
                $data = Purchase::find($id);
                $data->supplier_id = $request->supplier_id;
                $data->order_date = Carbon::parse($request->date);
                $data->due_date = Carbon::parse($request->due_date);
                $data->ref = $request->ref;
                $data->total = $request->total;
                $data->discount_type = $request->discount_type;
                $data->discount_value = $request->discount_value;
                $data->grand_total = $request->grand_total;
                $data->residual = $request->grand_total;
                $data->created_uid = auth()->user()->id;
                $data->save();

                
                foreach($request->lines as $i){
                    
                    if(array_key_exists("id", $i)){
                        $line_id = $i['id'];
                    }else{
                        $line_id = null;
                    }

                    $line = PurchaseLine::firstOrNew(['id' =>  $line_id]);
                    $line->investor_id = $i['investor_id'];
                    $line->percentage = $i['persentase'];
                    $line->mou_date = Carbon::parse($i['mou_date']);
                    $line->dob = Carbon::parse($i['dob']);
                    $line->jk = $i['jk'];
                    $line->type_id = $i['type_id'];
                    $line->weight = $i['weight'];
                    $line->price = $i['price'];
                    $line->company_id = $company_id;
                    $data->line()->save($line);
                }

                $remove = PurchaseLine::where('order_id', $data->id)->whereIn('id', $request->removedLines)->delete();

            }catch(\QueryException $e){
                DB::rollback();
                return back();
            }
            DB::commit();
            return redirect()->route('user.purchase.show', $data->id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }


    public function state(Request $request, $id)
    {    
        $company_id = auth()->user()->company_id;
        DB::beginTransaction();
        try{
            $po = Purchase::find($id);
            $po_line = PurchaseLine::where('order_id', $id)->get();
            if($request->state == 'confirm'){
                $stock = new StockMove();
                $stock->name = SquenceHelper::seq_stock_in($company_id);
                $stock->type = 'in';
                $stock->company_id = $company_id;
                $stock->partner_id = $po->supplier_id;
                $stock->location_id = $po->location_id;
                $stock->schedule_date = $po->schedule_date;
                $po->move()->save($stock);

                foreach($po_line as $l){
                    $sl = new StockMoveLine();
                    $sl->company_id = $company_id;
                    $sl->order_line_id = $l->id;
                    $stock->line()->save($sl);
                }

                $po->state = 'confirm';
                $po->save();

            }else{
                $po->state = 'cancel';
                $po->save();
            }

        }catch(\QueryException $e){
            DB::rollback();
            return back();
        }
        DB::commit();
        return redirect()->route('user.purchase.show', $id);
    }

    public function payment(Request $request)
    {
        // dd($request->all());
        $rules = [
            'amount' => 'required',
            'date' => 'required',
        ];

        $pesan = [
            'amount.required' => 'Jumlah Pembayaran Wajib Diisi!',
            'date.required' => 'Tanggal Pembayaran Wajib Diisi!',
        ];

        $validator = Validator::make($request->all(), $rules, $pesan);
        if ($validator->fails()){
            // return back()->withErrors($validator->errors());
            return response()->json([
                'fail' => true,
                'errors' => $validator->errors(),
            ]);
        }else{
            DB::beginTransaction();
            try{
                $po = Purchase::find($request->order_id);
                if($request->amount == $po->residual){
                    $po->status_payment = 'paid';
                }else{
                    $po->status_payment = 'partial';
                }
                $po->residual -= $request->amount;
                $po->save();


                $p = new Payment();
                $p->amount = $request->amount;
                $p->date = Carbon::parse($request->date);
                $p->company_id = auth()->user()->company_id;
                $p->created_uid = auth()->user()->id;
                $po->payment()->save($p);
                
            }catch(\QueryException $e){
                DB::rollback();
                return back();
            }
            DB::commit();
            return response()->json([
                'fail' => false
            ]);
        }
    }
    
    public function data(Request $request)
    {
        $sort = !empty($request->sort) ? $request->sort : 'id';
        $sortDir = !empty($request->sortDir) ? $request->sortDir : 'desc';
        $limit = ($request->limit) ? $request->limit : 25;

        $company_id = auth()->user()->company_id;
        
        $state = $request->state;
        $payment = $request->status_payment;
        $search = $request->search;
        $search_type = $request->searchType;
        // dd($relawan_id);

        $query = Purchase::join("res_partner as pr", function($join){
            $join->on("pr.id", "=", "purchase.supplier_id");
        })
        ->when($search_type, function($query, $search_type) use ($search){
            $query->where($search_type, 'LIKE', '%' . $search . '%');
        })
        ->when($state, function($query, $state){
            $query->where('state', '=', $state);
        })
        ->when($payment, function($query, $payment){
            $query->where('status_payment', '=', $payment);
        })
        ->select("purchase.*", "pr.name as supplier")
        ->where("purchase.company_id", "=", $company_id);
        
        if($request->page){
            $data = $query->paginate($limit);
        }else{
            $data = $query->get();
        }
        
        return response()->json($data);
    }
}