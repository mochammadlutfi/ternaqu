<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

use App\Models\Partner;
class InvestorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Inertia::render('Investor/Index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Investor/Form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'address' => 'required',
            'prov_id' => 'required',
            'kota_id' => 'required',
            'kec_id' => 'required',
            'kel_id' => 'required',
        ];

        $pesan = [
            'name.required' => 'Nama Investor Wajib Diisi!',
            'address.required' => 'Alamat Wajib Diisi!',
            'prov_id.required' => 'Provinsi Wajib Diisi!',
            'kota_id.required' => 'Kota/Kabupaten Wajib Diisi!',
            'kec_id.required' => 'Kecamatan Wajib Diisi!',
            'kel_id.required' => 'Kelurahan/Desa Wajib Diisi!',
        ];

        $validator = Validator::make($request->all(), $rules, $pesan);
        if ($validator->fails()){
            return back()->withErrors($validator->errors());
        }else{
            DB::beginTransaction();
            try{
                    $data = new Partner();
                    $data->name = $request->name;
                    $data->address = $request->address;
                    $data->prov_id = $request->prov_id;
                    $data->kota_id = $request->kota_id;
                    $data->kec_id = $request->kec_id;
                    $data->kel_id = $request->kel_id;
                    $data->postal_code = $request->postal_code;
                    $data->phone = $request->phone;
                    $data->telp = $request->telp;
                    $data->email = $request->email;
                    $data->type = 'investor';
                    $data->company_id = auth()->user()->company_id;
                    $data->save();

            }catch(\QueryException $e){
                DB::rollback();
                return back();
            }
            DB::commit();
            return redirect()->route('user.purchase.investor.show', $data->id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Partner::join("res_provinsi as pr", function($join){
            $join->on("pr.id", "=", "res_partner.prov_id");
        })
        ->join("res_kota as kt", function($join){
            $join->on("kt.id", "=", "res_partner.kota_id");
        })
        ->join("res_kecamatan as kec", function($join){
            $join->on("kec.id", "=", "res_partner.kec_id");
        })
        ->join("res_kelurahan as kel", function($join){
            $join->on("kel.id", "=", "res_partner.kel_id");
        })
        ->select("res_partner.*", "pr.nama as prov", "kt.nama as kota", "kec.nama as kec", "kel.nama as kel",
            DB::Raw("(SELECT SUM(amount) FROM account_invest WHERE investor_id=res_partner.id) as invest")
        )
        ->where('res_partner.id', $id)->first();
        
        return Inertia::render('Investor/Show',[
            'data' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Partner::where('id', $id)->first();

        return Inertia::render('Investor/Form',[
            'editMode' => true,
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required',
            'address' => 'required',
            'prov_id' => 'required',
            'kota_id' => 'required',
            'kec_id' => 'required',
            'kel_id' => 'required',
        ];

        $pesan = [
            'name.required' => 'Nama Supplier Wajib Diisi!',
            'address.required' => 'Alamat Wajib Diisi!',
            'prov_id.required' => 'Provinsi Wajib Diisi!',
            'kota_id.required' => 'Kota/Kabupaten Wajib Diisi!',
            'kec_id.required' => 'Kecamatan Wajib Diisi!',
            'kel_id.required' => 'Kelurahan/Desa Wajib Diisi!',
        ];

        $validator = Validator::make($request->all(), $rules, $pesan);
        if ($validator->fails()){
            return back()->withErrors($validator->errors());
        }else{
            DB::beginTransaction();
            try{
                    $data = Partner::find($id);
                    $data->name = $request->name;
                    $data->address = $request->address;
                    $data->prov_id = $request->prov_id;
                    $data->kota_id = $request->kota_id;
                    $data->kec_id = $request->kec_id;
                    $data->kel_id = $request->kel_id;
                    $data->postal_code = $request->postal_code;
                    $data->phone = $request->phone;
                    $data->telp = $request->telp;
                    $data->email = $request->email;
                    $data->type = 'investor';
                    $data->save();

            }catch(\QueryException $e){
                DB::rollback();
                return back();
            }
            DB::commit();
            return redirect()->route('user.purchase.investor.index')->with('message', 'Data Supllier Berhasil Diperbaharui!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    
    public function data(Request $request)
    {
        $sort = !empty($request->sort) ? $request->sort : 'id';
        $sortDir = !empty($request->sortDir) ? $request->sortDir : 'desc';
        $limit = ($request->limit) ? $request->limit : 25;

        $relawan_id = $request->relawan_id;
        $company_id = auth()->user()->company_id;

        // dd($relawan_id);

        $query = DB::table("res_partner as p")
        ->join("res_provinsi as pr", function($join){
            $join->on("pr.id", "=", "p.prov_id");
        })
        ->join("res_kota as kt", function($join){
            $join->on("kt.id", "=", "p.kota_id");
        })
        ->join("res_kecamatan as kec", function($join){
            $join->on("kec.id", "=", "p.kec_id");
        })
        ->join("res_kelurahan as kel", function($join){
            $join->on("kel.id", "=", "p.kel_id");
        })
        ->select("p.id", "p.name", "p.ref", "p.address", "p.email", "p.phone", "pr.nama as prov", "kt.nama as kota", "kec.nama as kec", "kel.nama as kel")
        ->where("p.type", "=", 'investor')
        ->where("p.company_id", "=", $company_id);
        
        if($request->page){
            $data = $query->paginate($limit);
        }else{
            $data = $query->get();
        }
        
        return response()->json($data);
    }
}
