<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\DB;
use Storage;
use Carbon\Carbon;


use App\Models\Cattle;

class CattleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Inertia::render('Cattle/Index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    
    public function data(Request $request)
    {
        $sort = !empty($request->sort) ? $request->sort : 'id';
        $sortDir = !empty($request->sortDir) ? $request->sortDir : 'desc';
        $limit = ($request->limit) ? $request->limit : 25;

        $id = $request->id;
        $code = $request->code;
        $company_id = auth()->user()->company_id;

        // dd($relawan_id);
        $query = DB::table("res_cattle as c")
        ->join("res_type as t", function($join){
            $join->on("t.id", "=", "c.type_id");
        })
        ->leftJoin("location as l", function($join){
            $join->on("l.id", "=", "c.location_id");
        })
        ->leftJoin("res_partner as rp", function($join){
            $join->on("rp.id", "=", "c.investor_id");
        })
        ->when($id, function($query, $id){
            $query->where('c.id', '=', $id);
        })
        ->when($code, function($query, $code){
            $query->where('c.eartag', '=', $code);
        })
        ->select("c.id", "c.eartag", "c.current_weight as weight", "c.kodisi", "c.jk", "c.image", "t.name as type", "l.name as location", "c.investor_id", "rp.name as investor")
        ->where("c.company_id", "=", $company_id);
        
        if($limit == 1){
            $data = $query->first();
        }else{
            if($request->page){
                $data = $query->paginate($limit);
            }else{
                $data = $query->get();
            }
        }
        
        return response()->json($data);
    }
}
