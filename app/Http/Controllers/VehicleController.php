<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\DB;
use Storage;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Models\Vehicle;
class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Inertia::render('Inventory/Vehicle');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'number' => 'required',
            'name' => 'required',
            'max_weight' => 'required',
        ];

        $pesan = [
            'number.required' => 'No Polisi Armada Wajib Diisi!',
            'name.required' => 'Model Armada Wajib Diisi!',
            'max_weight.required' => 'Kapasitas Armada Wajib Diisi!',
        ];

        $validator = Validator::make($request->all(), $rules, $pesan);
        if ($validator->fails()){
            return response()->json([
                'fail' => true,
                'errors' => $validator->errors(),
            ]);
        }else{
            DB::beginTransaction();
            try{
                $data = new Vehicle();
                $data->number = $request->number;
                $data->name = $request->name;
                $data->max_weight = $request->max_weight;
                $data->company_id = auth()->user()->company_id;
                $data->save();

            }catch(\QueryException $e){
                DB::rollback();
                return back();
            }
            DB::commit();
            return response()->json([
                'fail' => false
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Vehicle::find($id);
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'number' => 'required',
            'name' => 'required',
            'max_weight' => 'required',
        ];

        $pesan = [
            'number.required' => 'No Polisi Armada Wajib Diisi!',
            'name.required' => 'Model Armada Wajib Diisi!',
            'max_weight.required' => 'Kapasitas Armada Wajib Diisi!',
        ];

        $validator = Validator::make($request->all(), $rules, $pesan);
        if ($validator->fails()){
            return response()->json([
                'fail' => true,
                'errors' => $validator->errors(),
            ]);
        }else{
            DB::beginTransaction();
            try{
                $data = Vehicle::find($id);
                $data->number = $request->number;
                $data->name = $request->name;
                $data->max_weight = $request->max_weight;
                $data->company_id = auth()->user()->company_id;
                $data->save();

            }catch(\QueryException $e){
                DB::rollback();
                return back();
            }
            DB::commit();
            return response()->json([
                'fail' => false
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try{
            $data = Vehicle::find($id);

            if($data){
                $data->delete();
            }

        }catch(\QueryException $e){
            DB::rollback();
            return response()->json([
                'fail' => false
            ]);
        }
        DB::commit();
        return redirect()->route('user.inventory.vehicle.index');
    }

    
    public function data(Request $request)
    {
        $sort = !empty($request->sort) ? $request->sort : 'id';
        $sortDir = !empty($request->sortDir) ? $request->sortDir : 'desc';
        $limit = ($request->limit) ? $request->limit : 25;

        $id = $request->id;
        $company_id = auth()->user()->company_id;

        // dd($relawan_id);
        $query = DB::table("res_vehicle as rv")
        ->when($id, function($query, $id){
            $query->where('rv.id', '=', $id);
        })
        ->select("rv.*")
        ->where("rv.company_id", "=", $company_id);
        
        if($limit == 1){
            $data = $query->first();
        }else{
            if($request->page){
                $data = $query->paginate($limit);
            }else{
                $data = $query->get();
            }
        }
        
        return response()->json($data);
    }
}
