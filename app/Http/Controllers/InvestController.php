<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\DB;
use Storage;
use Carbon\Carbon;

use App\Helpers\SquenceHelper;
use Illuminate\Support\Facades\Validator;
use App\Models\Account\Bank;
use App\Models\Account\Invest;

class InvestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Inertia::render('Invest/Index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'investor_id' => 'required',
            'date' => 'required',
            'payment_method_id' => 'required',
            'amount' => 'required',
        ];

        $pesan = [
            'investor.required' => 'Investor Wajib Diisi!',
            'date.required' => 'Tanggal Wajib Diisi!',
            'payment_method_id.required' => 'Kode Wajib Diisi!',
            'amount.required' => 'Jumlah Wajib Diisi!',
        ];

        $validator = Validator::make($request->all(), $rules, $pesan);
        if ($validator->fails()){
            return response()->json([
                'fail' => true,
                'errors' => $validator->errors(),
            ]);
        }else{
            DB::beginTransaction();
            try{
                

                $data = new Invest();
                $data->name = SquenceHelper::seq_invest(auth()->user()->company_id);
                $data->investor_id = $request->investor_id;
                $data->date = Carbon::parse($request->date);
                $data->payment_method_id = $request->payment_method_id;
                $data->amount = $request->amount;
                $data->company_id = auth()->user()->company_id;
                $data->save();

            }catch(\QueryException $e){
                DB::rollback();
                return back();
            }
            DB::commit();
            return response()->json([
                'fail' => false
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table("account_invest as ai")
        ->join('res_partner as rp', 'rp.id', 'ai.investor_id')
        ->join('account_journal as aj', 'aj.id', 'ai.payment_method_id')
        ->select("ai.id", "ai.name", "ai.investor_id", "ai.date", "ai.payment_method_id", "ai.amount", "rp.name as investor", "aj.name as payment_method")
        ->where('ai.id', $id)->first();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'investor_id' => 'required',
            'date' => 'required',
            'payment_method_id' => 'required',
            'amount' => 'required',
        ];

        $pesan = [
            'investor.required' => 'Investor Wajib Diisi!',
            'date.required' => 'Tanggal Wajib Diisi!',
            'payment_method_id.required' => 'Kode Wajib Diisi!',
            'amount.required' => 'Jumlah Wajib Diisi!',
        ];

        $validator = Validator::make($request->all(), $rules, $pesan);
        if ($validator->fails()){
            return response()->json([
                'fail' => true,
                'errors' => $validator->errors(),
            ]);
        }else{
            DB::beginTransaction();
            try{
                $data = Invest::find($id);
                $data->investor_id = $request->investor_id;
                $data->date = Carbon::parse($request->date);
                $data->payment_method_id = $request->payment_method_id;
                $data->amount = $request->amount;
                $data->company_id = auth()->user()->company_id;
                $data->save();

            }catch(\QueryException $e){
                DB::rollback();
                return back();
            }
            DB::commit();
            return response()->json([
                'fail' => false
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try{
            $data = Journal::find($id);

            if($data){
                $data->delete();
            }

        }catch(\QueryException $e){
            DB::rollback();
            return response()->json([
                'fail' => false
            ]);
        }
        DB::commit();
        return redirect()->route('user.finance.payment_method.index');
    }

    
    public function data(Request $request)
    {
        $sort = !empty($request->sort) ? $request->sort : 'id';
        $sortDir = !empty($request->sortDir) ? $request->sortDir : 'desc';
        $limit = ($request->limit) ? $request->limit : 25;

        $id = $request->id;
        $company_id = auth()->user()->company_id;

        // dd($relawan_id);
        $query = DB::table("account_invest as ai")
        ->join('res_partner as rp', 'rp.id', 'ai.investor_id')
        ->join('account_journal as aj', 'aj.id', 'ai.payment_method_id')
        ->select("ai.id", "ai.name", "ai.investor_id", "ai.date", "ai.payment_method_id", "ai.amount", "rp.name as investor", "aj.name as payment_method")
        ->when($id, function($query, $id){
            $query->where('ai.id', '=', $id);
        })
        ->where("ai.company_id", "=", $company_id);
        
        if($limit == 1){
            $data = $query->first();
        }else{
            if($request->page){
                $data = $query->paginate($limit);
            }else{
                $data = $query->get();
            }
        }
        
        return response()->json($data);
    }

    
    public function total($id, Request $request)
    {
        $query = DB::table("account_invest as ai")
        ->join('res_partner as rp', 'rp.id', 'ai.investor_id')
        ->join('account_journal as aj', 'aj.id', 'ai.payment_method_id')
        ->select("ai.id", "ai.name", "ai.investor_id", "ai.date", "ai.payment_method_id", "ai.amount", "rp.name as investor", "aj.name as payment_method")
        ->when($id, function($query, $id){
            $query->where('ai.id', '=', $id);
        })
        ->where("ai.company_id", "=", $company_id);
        
        if($limit == 1){
            $data = $query->first();
        }else{
            if($request->page){
                $data = $query->paginate($limit);
            }else{
                $data = $query->get();
            }
        }
        
        return response()->json($data);
    }
}
