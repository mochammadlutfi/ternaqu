<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use App\Helpers\SquenceHelper;
use App\Models\Sale;
use App\Models\SaleLine;
use App\Models\StockMove;
use App\Models\StockMoveLine;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Inertia::render('Sale/Index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Sale/Form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $rules = [
            'customer_id' => 'required',
            'date' => 'required',
        ];

        $pesan = [
            'customer_id.required' => 'Pelanggan Wajib Diisi!',
            'date.required' => 'Tanggal Wajib Diisi!',
        ];

        $validator = Validator::make($request->all(), $rules, $pesan);
        if ($validator->fails()){
            return back()->withErrors($validator->errors());
        }else{
            DB::beginTransaction();
            try{
                $company_id = auth()->user()->company_id;
                $data = new Sale();
                $data->name = SquenceHelper::seq_sale($company_id);
                $data->customer_id = $request->customer_id;
                $data->order_date = Carbon::parse($request->date);
                $data->due_date = Carbon::parse($request->due_date);
                $data->schedule_date = Carbon::parse($request->schedule_date);
                $data->ref = $request->ref;
                $data->total = $request->total;
                $data->discount_type = $request->discount_type;
                $data->discount_value = $request->discount_value;
                $data->grand_total = $request->grand_total;
                $data->company_id = $company_id;
                $data->state = 'draft';
                $data->created_uid = auth()->user()->id;
                $data->save();

                foreach($request->lines as $i){
                    $line = new SaleLine();
                    $line->cattle_id = $i['cattle_id'];
                    $line->weight = $i['weight'];
                    $line->unit_price = $i['unit_price'];
                    $line->price = $i['price'];
                    $line->company_id = $company_id;
                    $data->line()->save($line);
                }

            }catch(\QueryException $e){
                DB::rollback();
                return back();
            }
            DB::commit();
            return redirect()->route('user.sale.show', $data->id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $company_id = auth()->user()->company_id;

        $data = Sale::with('payment')
        ->join("res_partner as pr","pr.id", "=", "sale.customer_id")
        ->select("sale.*", "pr.name as customer", 
            DB::Raw("(SELECT COUNT(*) FROM stock_move WHERE type='out' AND ordertable_id=sale.id) as pengiriman"),
            DB::Raw("(SELECT COUNT(*) FROM payments WHERE type='income' AND paymenttable_id=sale.id) as pembayaran")
        )
        ->where("sale.company_id", "=", $company_id)
        ->where('sale.id', $id)->first();

        $data->lines = SaleLine::
        join("res_cattle as rc", "rc.id", "=", "sale_line.cattle_id")
        ->join("res_type as rt", "rt.id", "=", "rc.type_id")
        ->join("location as rl", "rl.id", "=", "rc.location_id")
        ->select("sale_line.*", "rt.name as type", "rc.eartag", "rc.jk", "rc.current_weight as weight", "rc.image", "rl.name as location")
        ->where('sale_line.order_id', $data->id)->get();

        if(in_array($data->state, ['confirm', 'done'])){
            $data->move_id = StockMove::where('ordertable_type', 'App\Models\Sale')->where('ordertable_id', $data->id)->first()->id;
        }

        return Inertia::render('Sale/Show', [
            'data' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company_id = auth()->user()->company_id;

        $data = Sale::join("res_partner as pr", function($join){
            $join->on("pr.id", "=", "sale.supplier_id");
        })
        ->select("sale.*", "pr.name as supplier")
        ->where("sale.company_id", "=", $company_id)
        ->where('sale.id', $id)->first();

        $data->lines = SaleLine::join("res_type as rt", function($join){
            $join->on("rt.id", "=", "sale_line.type_id");
        })
        ->select("sale_line.*", "rt.name as type")
        ->where('sale_line.order_id', $data->id)->get();

        return Inertia::render('Sale/Form',[
            'editMode' => true,
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'supplier_id' => 'required',
            'date' => 'required',
        ];

        $pesan = [
            'supplier_id.required' => 'Supplier Wajib Diisi!',
            'date.required' => 'Tanggal Wajib Diisi!',
        ];

        $validator = Validator::make($request->all(), $rules, $pesan);
        if ($validator->fails()){
            return back()->withErrors($validator->errors());
        }else{
            DB::beginTransaction();
            try{
                $company_id = auth()->user()->company_id;
                $data = Sale::find($id);
                $data->supplier_id = $request->supplier_id;
                $data->order_date = Carbon::parse($request->date);
                $data->due_date = Carbon::parse($request->due_date);
                $data->ref = $request->ref;
                $data->total = $request->total;
                $data->discount_type = $request->discount_type;
                $data->discount_value = $request->discount_value;
                $data->grand_total = $request->grand_total;
                $data->created_uid = auth()->user()->id;
                $data->save();

                
                foreach($request->lines as $i){
                    
                    if(array_key_exists("id", $i)){
                        $line_id = $i['id'];
                    }else{
                        $line_id = null;
                    }

                    $line = SaleLine::firstOrNew(['id' =>  $line_id]);
                    $line->eartag = $i['eartag'];
                    $line->type_id = $i['type_id'];
                    $line->weight = $i['weight'];
                    $line->price = $i['price'];
                    $line->company_id = $company_id;
                    $data->line()->save($line);
                }

                $remove = SaleLine::where('order_id', $data->id)->whereIn('id', $request->removedLines)->delete();

            }catch(\QueryException $e){
                DB::rollback();
                return back();
            }
            DB::commit();
            return redirect()->route('user.sale.show', $data->id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }


    public function state(Request $request, $id)
    {    
        $company_id = auth()->user()->company_id;
        DB::beginTransaction();
        try{
            $po = Sale::find($id);
            $po_line = SaleLine::where('order_id', $id)->get();
            if($request->state == 'confirm'){
                $stock = new StockMove();
                $stock->name = SquenceHelper::seq_stock_out($company_id);
                $stock->type = 'out';
                $stock->company_id = $company_id;
                $stock->partner_id = $po->customer_id;
                $stock->schedule_date = $po->schedule_date;
                $po->move()->save($stock);

                foreach($po_line as $l){
                    $sl = new StockMoveLine();
                    $sl->company_id = $company_id;
                    $sl->order_line_id = $l->id;
                    $stock->line()->save($sl);
                }

                $po->state = 'confirm';
                $po->save();

            }else{
                $po->state = 'cancel';
                $po->save();
            }

        }catch(\QueryException $e){
            DB::rollback();
            return back();
        }
        DB::commit();
        return redirect()->route('user.sale.show', $id);
    }

    
    public function data(Request $request)
    {
        $sort = !empty($request->sort) ? $request->sort : 'id';
        $sortDir = !empty($request->sortDir) ? $request->sortDir : 'desc';
        $limit = ($request->limit) ? $request->limit : 25;

        $company_id = auth()->user()->company_id;
        
        $state = $request->state;
        $payment = $request->payment;
        $search = $request->search;
        $search_type = $request->searchType;
        // dd($relawan_id);

        $query = Sale::join("res_partner as pr", "pr.id", "=", "sale.customer_id")
        ->when($search_type, function($query, $search_type) use ($search){
            $query->where($search_type, 'LIKE', '%' . $search . '%');
        })
        ->when($state, function($query, $state){
            $query->where('state', '=', $state);
        })
        ->when($payment, function($query, $payment){
            $query->where('status_payment', '=', $payment);
        })
        ->select("sale.*", "pr.name as customer")
        ->where("sale.company_id", "=", $company_id);
        
        if($request->page){
            $data = $query->paginate($limit);
        }else{
            $data = $query->get();
        }
        
        return response()->json($data);
    }

    public function chart(Request $request)
    {
        
    }
}