<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class BaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function wilayah(Request $request)
    {
        $type = $request->type;
        $search = $request->search;
        $parent_id = $request->parent_id;
        $id = $request->id;

        if($type == 'provinsi'){
            $data = DB::table("res_provinsi as a")->orderBy('nama', 'ASC')->get();
        }elseif($type == 'kota'){
            $data = DB::table("res_kota as a")
            ->when($id, function($query, $id){
                $query->where('id', $id);
            })
            ->when($parent_id, function($query, $parent_id){
                $query->where('prov_id', $parent_id);
            })
            ->orderBy('nama', 'ASC')->get();
        }elseif($type == 'kecamatan'){
            $data = DB::table("res_kecamatan as a")
            ->when($id, function($query, $id){
                $query->where('id', $id);
            })
            ->when($parent_id, function($query, $parent_id){
                $query->where('kota_id', $parent_id);
            })->orderBy('nama', 'ASC')->get();
        }else{
            $data = DB::table("res_kelurahan as a")
            ->when($id, function($query, $id){
                $query->where('id', $id);
            })
            ->when($parent_id, function($query, $parent_id){
                $query->where('kec_id', $parent_id);
            })
            ->orderBy('nama', 'ASC')->get();
        }
        
        return response()->json($data);
    }

    public function type(Request $request){

        $id = $request->id;
        
        $data = DB::table("res_type as a")
        ->when($id, function($query, $id){
            $query->where('id', $id);
        })
        ->orderBy('name', 'ASC')->get();
        
        return response()->json($data);
    }

    public function menu(Request $request)
    {
        $data = Collect([
            [
                'name' => 'Supplier',
                'icon_type' => 'font',
                'icon_src' => 'si si-users',
                'route' => route('user.purchase.supplier.index'),
            ],
            [
                'name' => 'Investor',
                'icon_type' => 'font',
                'icon_src' => 'si si-users',
                'route' => route('user.purchase.investor.index'),
            ],
            [
                'name' => 'Pelanggan',
                'icon_type' => 'font',
                'icon_src' => 'si si-users',
                'route' =>  route('user.sale.customer.index'),
            ],
            [
                'name' => 'Pengiriman',
                'icon_type' => 'font',
                'icon_src' => 'fa fa-truck',
                'route' =>  route('user.inventory.index', ['state' => 'out']),
            ],
            [
                'name' => 'Penerimaan',
                'icon_type' => 'font',
                'icon_src' => 'fa fa-truck-loading',
                'route' =>  route('user.inventory.index', ['state' => 'in']),
            ],
            [
                'name' => 'Lokasi & Pent',
                'icon_type' => 'font',
                'icon_src' => 'fa fa-warehouse',
                'route' =>  route('user.inventory.location.index'),
            ],
            [
                'name' => 'Unit Armada',
                'icon_type' => 'font',
                'icon_src' => 'fa fa-car',
                'route' =>  route('user.inventory.vehicle.index'),
            ],
            [
                'name' => 'List Ternak',
                'icon_type' => 'image',
                'icon_src' => '/images/icons/cow.svg',
                'route' =>  route('user.inventory.cattle.index'),
            ],
            [
                'name' => 'Medical Check',
                'icon_type' => 'font',
                'icon_src' => 'fa fa-syringe',
                'route' =>  route('user.medical.index'),
            ],
            [
                'name' => 'Kas Operasional',
                'icon_type' => 'font',
                'icon_src' => 'fa fa-money-bill-alt',
                'route' =>  route('user.finance.cash.index'),
            ],
            [
                'name' => 'Investasi',
                'icon_type' => 'font',
                'icon_src' => 'fas fa-hand-holding-usd',
                'route' =>  route('user.finance.invest.index'),
            ],
            [
                'name' => 'Metode Pembayaran',
                'icon_type' => 'font',
                'icon_src' => 'fa fa-funnel-dollar',
                'route' =>  route('user.finance.payment_method.index'),
            ],
        ]);

        return response()->json($data->all());

    }

}