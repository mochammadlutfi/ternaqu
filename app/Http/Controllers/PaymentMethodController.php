<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\DB;
use Storage;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Models\Account\Bank;
use App\Models\Account\Journal;

class PaymentMethodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Inertia::render('Payment/PaymentMethod');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'type' => 'required',
            'name' => 'required',
            'code' => 'required',
        ];

        $pesan = [
            'type.required' => 'Tipe Wajib Diisi!',
            'name.required' => 'Nama Wajib Diisi!',
            'code.required' => 'Kode Wajib Diisi!',
        ];

        $validator = Validator::make($request->all(), $rules, $pesan);
        if ($validator->fails()){
            return response()->json([
                'fail' => true,
                'errors' => $validator->errors(),
            ]);
        }else{
            DB::beginTransaction();
            try{
                if($request->type == 'Bank'){
                    $bk = new Bank();
                    $bk->name = $request->name;
                    $bk->number = $request->number;
                    $bk->company_id = auth()->user()->company_id;
                    $bk->save();
                }
                $data = new Journal();
                $data->type = $request->type;
                $data->name = $request->name;

                if($request->type == 'Bank'){
                    $data->bank_id = $bk->bank_id;
                }
                $data->company_id = auth()->user()->company_id;
                $data->save();

            }catch(\QueryException $e){
                DB::rollback();
                return back();
            }
            DB::commit();
            return response()->json([
                'fail' => false
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table("account_journal as aj")
        ->leftjoin('account_bank as ab', 'ab.id', 'aj.bank_id')
        ->select("aj.id", "aj.name", "aj.code", "aj.bank_id", "ab.number", "aj.type")
        ->where('aj.id', $id)->first();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'type' => 'required',
            'name' => 'required',
            'code' => 'required',
        ];

        $pesan = [
            'type.required' => 'Tipe Wajib Diisi!',
            'name.required' => 'Nama Wajib Diisi!',
            'code.required' => 'Kode Wajib Diisi!',
        ];

        $validator = Validator::make($request->all(), $rules, $pesan);
        if ($validator->fails()){
            return response()->json([
                'fail' => true,
                'errors' => $validator->errors(),
            ]);
        }else{
            DB::beginTransaction();
            try{
                if($request->type == 'Bank'){
                    $bk = Bank::firstOrNew(['id' =>  $request->bank_id]);
                    $bk->name = $request->name;
                    $bk->number = $request->number;
                    $bk->company_id = auth()->user()->company_id;
                    $bk->save();
                }

                $data = new Journal();
                $data->type = $request->type;
                $data->name = $request->name;

                if($request->type == 'Bank'){
                    $data->bank_id = $bk->id;
                }
                $data->company_id = auth()->user()->company_id;
                $data->save();

            }catch(\QueryException $e){
                DB::rollback();
                return back();
            }
            DB::commit();
            return response()->json([
                'fail' => false
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try{
            $data = Journal::find($id);

            if($data){
                $data->delete();
            }

        }catch(\QueryException $e){
            DB::rollback();
            return response()->json([
                'fail' => false
            ]);
        }
        DB::commit();
        return redirect()->route('user.finance.payment_method.index');
    }

    
    public function data(Request $request)
    {
        $sort = !empty($request->sort) ? $request->sort : 'id';
        $sortDir = !empty($request->sortDir) ? $request->sortDir : 'desc';
        $limit = ($request->limit) ? $request->limit : 25;

        $id = $request->id;
        $company_id = auth()->user()->company_id;

        // dd($relawan_id);
        $query = DB::table("account_journal as aj")
        ->leftjoin('account_bank as ab', 'ab.id', 'aj.bank_id')
        ->when($id, function($query, $id){
            $query->where('aj.id', '=', $id);
        })
        ->select("aj.id", "aj.name", "aj.code", "aj.bank_id", "ab.number", "aj.type")
        ->where("aj.company_id", "=", $company_id);
        
        if($limit == 1){
            $data = $query->first();
        }else{
            if($request->page){
                $data = $query->paginate($limit);
            }else{
                $data = $query->get();
            }
        }
        
        return response()->json($data);
    }
}
