<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Inertia\Inertia;
use Illuminate\Support\Facades\Validator;


class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return Inertia::render('Dashboard/Index');
    }

    public function counter(Request $request){
        
        $company_id = auth()->user()->company_id;
        
        $data = Collect([
            'sale' => DB::table('sale')->where('company_id', $company_id)->count(),
            'purchase' => DB::table('purchase')->where('company_id', $company_id)->count(),
            'customer' => DB::table('res_partner')->where('type', 'Customer')->where('company_id', $company_id)->count(),
            'cattle' => DB::table('res_cattle')->where('company_id', $company_id)->count(),
        ]);

        return response()->json($data);
    }

}