<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;

use DB;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
class ReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Get Wilayah
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        
        $start_date = Carbon::now()->startOfYear();
        $end_date = Carbon::now()->addMonth(2);
        // // $data = DB::connection('pgsql')->table("account_invoice")
        // // ->get();
        $i = 0;
        $label = array();
        $vb = array();
        // $period = CarbonPeriod::create(Carbon::now()->startOfMonth(), Carbon::now());
        
        $period = CarbonPeriod::create($start_date, '1 month', $end_date);

        // foreach ($period as $dt) {
        //         // echo $dt->format("F") . "<br>\n";
        // }

        foreach($period as $p){
            $label[] = $p->translatedFormat('F');
            $vb[] = DB::connection('pgsql')->table("account_invoice")->where('type', 'in_invoice')
            ->whereMonth('date_invoice', $p)->get()->sum('amount_total');
        }

        // return response()->json([
        //     "label" => $label,
        //     "dataset" => $rekrutan,
        // ]);

        // // return response()->json($data);
        return Inertia::render('Report', [
            'labels' => $label,
            'value' => $vb,
        ]);
    }

    
}
