<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Storage;
use Carbon\Carbon;
use App\Models\StockMove;
use App\Models\StockMoveLine;
use App\Models\Cattle;
use App\Models\Purchase;

use Image;

class StockMoveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($state)
    {
        if($state == 'in'){
            return Inertia::render('Inventory/Receipt');
        }elseif($state == 'out'){
            return Inertia::render('Inventory/Delivery');
        }else{
            return Inertia::render('Inventory/Transfer');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Supplier/Form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'address' => 'required',
            'prov_id' => 'required',
            'kota_id' => 'required',
            'kec_id' => 'required',
            'kel_id' => 'required',
        ];

        $pesan = [
            'name.required' => 'Nama Supplier Wajib Diisi!',
            'address.required' => 'Alamat Wajib Diisi!',
            'prov_id.required' => 'Provinsi Wajib Diisi!',
            'kota_id.required' => 'Kota/Kabupaten Wajib Diisi!',
            'kec_id.required' => 'Kecamatan Wajib Diisi!',
            'kel_id.required' => 'Kelurahan/Desa Wajib Diisi!',
        ];

        $validator = Validator::make($request->all(), $rules, $pesan);
        if ($validator->fails()){
            return back()->withErrors($validator->errors());
        }else{
            DB::beginTransaction();
            try{
                    $data = new Supplier();
                    $data->name = $request->name;
                    $data->address = $request->address;
                    $data->prov_id = $request->prov_id;
                    $data->kota_id = $request->kota_id;
                    $data->kec_id = $request->kec_id;
                    $data->kel_id = $request->kel_id;
                    $data->postal_code = $request->postal_code;
                    $data->phone = $request->phone;
                    $data->telp = $request->telp;
                    $data->email = $request->email;
                    $data->type = 'supplier';
                    $data->company_id = auth()->user()->company_id;
                    $data->save();

            }catch(\QueryException $e){
                DB::rollback();
                return back();
            }
            DB::commit();
            return redirect()->route('admin.product.brand.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($state, $id)
    {
        if($state == 'in'){

            $data = DB::table("stock_move as sp")
            ->join("res_partner as rp", function($join){
                $join->on("rp.id", "=", "sp.partner_id");
            })
            ->leftjoin("location as rl", function($join){
                $join->on("rl.id", "=", "sp.location_id");
            })
            ->join("purchase as sc", function($join){
                $join->on("sc.id", "=", "sp.ordertable_id");
            })
            ->select("sp.*", "rp.name as partner", "rl.name as location", "sc.name as source")
            ->where('sp.id', $id)
            ->first();
    
            $data->lines = DB::table("stock_move_line as ml")
            ->join('purchase_line as scl', 'scl.id', '=' , 'ml.order_line_id')
            ->join("res_type as rt", "rt.id", '=', 'scl.type_id')
            ->leftjoin("res_partner as rp", "rp.id", '=', 'scl.investor_id')
            ->select('ml.id', 'ml.state', 'ml.image', "ml.eartag", "ml.ref", "scl.investor_id", "rp.name as investor", "scl.weight", "rt.name as type", "scl.jk", "scl.dob", "scl.mou_date")
            ->where('move_id', $data->id)
            ->get();

            $view = 'Inventory/ReceiptShow';
        }elseif($state == 'out'){

            $data = DB::table("stock_move as sp")
            ->join("res_partner as rp", function($join){
                $join->on("rp.id", "=", "sp.partner_id");
            })
            ->leftjoin("location as rl", function($join){
                $join->on("rl.id", "=", "sp.location_id");
            })
            ->leftjoin("res_vehicle as rv", function($join){
                $join->on("rv.id", "=", "sp.vehicle_id");
            })
            ->join("sale as sc", function($join){
                $join->on("sc.id", "=", "sp.ordertable_id");
            })
            ->select("sp.*", "rp.name as partner", "rl.name as location", "sc.name as source",
            DB::raw("CONCAT(rv.number,' - ',rv.name) as armada"))
            ->where('sp.id', $id)
            ->first();

            $data->lines = StockMoveLine::with([
                'cattle' => function($q){
                    return $q->select('res_cattle.id', 'res_cattle.image', 'eartag', 'current_weight as weight', 'jk', 'l.name as location', 'lp.name as pent')
                    ->join('location as l', 'l.id', '=', 'res_cattle.location_id')
                    ->join('location_pents as lp', 'lp.id', '=', 'res_cattle.pent_id');
                }
            ])
            ->join('sale_line as scl', 'scl.id', '=' , 'stock_move_line.order_line_id')
            ->select('stock_move_line.id', "stock_move_line.cattle_id", 'stock_move_line.state', "scl.price")
            ->where('move_id', $data->id)
            ->get();
            $view = 'Inventory/DeliveryShow';
        }else{
            $view = 'Inventory/ReceiptShow';
        }
        
        return Inertia::render($view,[
            'data' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Supplier::where('id', $id)->first();

        return Inertia::render('Supplier/Form',[
            'editMode' => true,
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $state, $id)
    {
        // dd($request->all());
        if($state == 'out'){
            $rules = [];
        }else{
            $rules = [];
            $pesan = [];
        }

        $validator = Validator::make($request->all(), $rules, $pesan);
        if ($validator->fails()){
            return back()->withErrors($validator->errors());
        }else{
            DB::beginTransaction();
            try{

                $lines = $request->lines;
                $sm = StockMove::find($id);

                $i = 0;
                if($state == 'in'){
                    $sm->ref = $request->ref;
                    $sm->confirmed_date = Carbon::now();
                    $sm->confirmed_uid = auth()->user()->id;
                    $sm->state = 1;
                    $sm->save();

                    $po = Purchase::where('id', $sm->ordertable_id)->first();
                    $po->state = 'done';
                    $po->save();
    
                    $qlm = DB::table("stock_move_line as ml")
                    ->join('purchase_line as scl', 'scl.id', '=' , 'ml.order_line_id')
                    ->join("res_type as rt", "rt.id", '=', 'scl.type_id')
                    ->leftjoin("res_partner as rp", "rp.id", '=', 'scl.investor_id')
                    ->select('ml.id', 'ml.state', 'ml.image', "scl.eartag", "scl.weight", "rt.name as type", "scl.investor_id", "rp.name as investor", "scl.type_id")
                    ->where('move_id', $id)
                    ->get();
                    foreach($qlm as $l){
                        if($lines[$i]['image']){
                            $img = $this->uploadImage($lines[$i]['image'], auth()->user()->company_id);
                        }

                        $lm = StockMoveLine::find($lines[$i]['id']);
                        $lm->state = $lines[$i]['state'];
                        $lm->image = $img;
                        $lm->eartag = $lines[$i]['eartag'];
                        $lm->save();
    
                        if($lines[$i]['state']){
                            $ls = new Cattle();
                            $ls->company_id = auth()->user()->company_id;
                            $ls->type_id = $l->type_id;
                            $ls->location_id = $sm->location_id;
                            $ls->eartag = $lines[$i]['eartag'];
                            $ls->start_weight = $l->weight;
                            $ls->current_weight = $l->weight;
                            $ls->move_line_id = $l->id;
                            $ls->image = $img;
                            $ls->investor_id = $l->investor_id;
                            $ls->state = 0;
                            $ls->save();
                        }
                        $i++;
                    }
                }elseif($state == 'out'){
                    
                    $sm->ref = $request->ref;
                    $sm->confirmed_date = Carbon::now();
                    $sm->confirmed_uid = auth()->user()->id;
                    $sm->state = 1;
                    $sm->save();

                    $qlm = DB::table("stock_move_line as ml")
                    ->join('sale_line as scl', 'scl.id', '=' , 'ml.order_line_id')
                    ->join("res_cattle as rc", "rc.id", '=', 'scl.cattle_id')
                    ->select('ml.id', 'ml.state', 'ml.image', "rc.eartag")
                    ->where('move_id', $id)
                    ->get();
                    foreach($qlm as $l){

                        $lm = StockMoveLine::find($lines[$i]['id']);
                        $lm->cattle_id = $lines[$i]['cattle_id'];
                        $lm->eartag = $lines[$i]['eartag'];
                        $lm->weight = $lines[$i]['weight'];
                        $lm->jk = $lines[$i]['jk'];
                        $lm->state = ($lines[$i]['state']) ? 1 : 0;
                        $lm->save();

                        $lc = Cattle::find($lines[$i]['cattle_id']);
                        $lc->state = 0;
                        $lc->save();
                        $i++;
                    }
                }

            }catch(\QueryException $e){
                DB::rollback();
                return back();
            }
            DB::commit();
            return redirect()->route('user.inventory.show', ['state' => $state, 'id' => $id])->with('message', 'Data Supllier Berhasil Diperbaharui!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    private function uploadImage($file, $company_id){
        $file_name = uniqid() . '.' . $file->getClientOriginalExtension();

        $imgFile = Image::make($file->getRealPath());

        $imgFile->resize(800, 800, function ($constraint) {
		    $constraint->aspectRatio();
		})->encode('jpg', 80);

        
        Storage::disk('public')->putFileAs($company_id.'/cattle',
            $file,
            $file_name
        );
        
        return '/uploads/'.$company_id.'/cattle/'.$file_name;
    }

    
    public function data(Request $request)
    {
        $sort = !empty($request->sort) ? $request->sort : 'id';
        $sortDir = !empty($request->sortDir) ? $request->sortDir : 'desc';
        $limit = ($request->limit) ? $request->limit : 25;

        $company_id = auth()->user()->company_id;
        $type = $request->type;
        $source = $request->source;

        $query = DB::table("stock_move as sp")
        ->join("res_partner as rp", function($join){
            $join->on("rp.id", "=", "sp.partner_id");
        })
        ->leftjoin("stock_move as par", function($join){
            $join->on("par.id", "=", "sp.back_order_id");
        })
        ->when($type == 'in', function($query){
            $query->join("purchase as tr", function($join){
                $join->on("tr.id", "=", "sp.ordertable_id");
            });
        })
        ->when($type == 'out', function($query){
            $query->join("sale as tr", function($join){
                $join->on("tr.id", "=", "sp.ordertable_id");
            });
        })
        ->when($source, function($query, $source){
            $query->where('tr.id', '=', $source);
        })
        ->select("sp.id", "sp.name", 'sp.schedule_date', 'sp.state', 'rp.name as partner', 'tr.name as source', 'par.name as parent')
        ->where("sp.type", "=", $type)
        ->where("sp.company_id", "=", $company_id);
        
        if($request->page){
            $data = $query->paginate($limit);
        }else{
            $data = $query->get();
        }
        
        return response()->json($data);
    }
}
