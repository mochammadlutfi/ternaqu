<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Storage;
use Image;

use App\Models\Cattle;
use App\Models\MedicalCheck;
class MedicalCheckController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Inertia::render('MedicalCheck/Index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('MedicalCheck/Form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'cattle_id' => 'required',
            'condition' => 'required',
            'weight' => 'required',
            'image' => 'required',
        ];

        $pesan = [
            'cattle_id.required' => 'Ternak Wajib Diisi!',
            'condition.required' => 'Kondisi Ternak Wajib Diisi!',
            'weight.required' => 'Berat Ternak Diisi!',
            'image.required' => 'Foto Ternak Wajib Diisi!',
        ];

        $validator = Validator::make($request->all(), $rules, $pesan);
        if ($validator->fails()){
            return back()->withErrors($validator->errors());
        }else{
            DB::beginTransaction();
            try{
                    $data = new MedicalCheck();
                    $data->date = Carbon::parse($request->date);
                    $data->cattle_id = $request->cattle_id;
                    $data->status = $request->condition;
                    $data->weight = $request->weight;
                    $data->description = $request->description;
                    $data->image = $this->uploadImage($request->image, auth()->user()->company_id);
                    $data->company_id = auth()->user()->company_id;
                    $data->save();

                   $cattle = Cattle::where('id', $data->cattle_id)->first();
                   $cattle->current_weight = $data->weight;
                   $cattle->image = $data->image;
                //    $cattle->kondisi = ($data->status == 'Sehat') ? 1 : 0;
                   $cattle->save();
            }catch(\QueryException $e){
                DB::rollback();
                return back();
            }
            DB::commit();
            return redirect()->route('user.medical.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = DB::table("medical_check as mc")
        ->join('res_cattle as rc', 'rc.id', '=', 'mc.cattle_id')
        ->join('res_type as rt', 'rt.id', '=', 'rc.type_id')
        ->join('location as l', 'l.id', '=', 'rc.location_id')
        ->leftJoin('res_partner as rp', 'rp.id', '=', 'rc.investor_id')
        ->select("mc.id", "mc.date", "mc.weight", "mc.status", "mc.description", "mc.image", "rc.eartag", "rt.name as type", "l.name as location", "rc.jk", "rp.name as investor")
        ->where('mc.id', $id)
        ->first();
        
        return Inertia::render('MedicalCheck/Show', [
            'data' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table("medical_check as mc")
        ->join('res_cattle as rc', 'rc.id', '=', 'mc.cattle_id')
        ->join('res_type as rt', 'rt.id', '=', 'rc.type_id')
        ->join('location as l', 'l.id', '=', 'rc.location_id')
        ->leftJoin('res_partner as rp', 'rp.id', '=', 'rc.investor_id')
        ->select("mc.id", "mc.date", "mc.weight", "mc.status", "mc.description", "mc.image", "rc.eartag", "rt.name as type", "l.name as location", "rc.jk", "rp.name as investor")
        ->where('mc.id', $id)
        ->first();

        return Inertia::render('MedicalCheck/Form',[
            'editMode' => true,
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'cattle_id' => 'required',
            'condition' => 'required',
            'weight' => 'required',
            'image' => 'required',
        ];

        $pesan = [
            'cattle_id.required' => 'Ternak Wajib Diisi!',
            'condition.required' => 'Kondisi Ternak Wajib Diisi!',
            'weight.required' => 'Berat Ternak Diisi!',
            'image.required' => 'Foto Ternak Wajib Diisi!',
        ];

        $validator = Validator::make($request->all(), $rules, $pesan);
        if ($validator->fails()){
            return back()->withErrors($validator->errors());
        }else{
            DB::beginTransaction();
            try{
                    $data = MedicalCheck::find($id);
                    $data->date = Carbon::parse($request->date);
                    $data->cattle_id = $request->cattle_id;
                    $data->status = $request->condition;
                    $data->weight = $request->weight;
                    $data->description = $request->description;
                    $data->image = $this->uploadImage($request->image, auth()->user()->company_id);
                    $data->save();

                   $cattle = Cattle::where('id', $data->cattle_id)->first();
                   $cattle->current_weight = $data->weight;
                   $cattle->image = $data->image;
                   $cattle->kondisi = ($data->status == 'Sehat') ? 1 : 0;
                   $cattle->save();

            }catch(\QueryException $e){
                DB::rollback();
                return back();
            }
            DB::commit();
            return redirect()->route('user.medical.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function uploadImage($file, $company_id){
        $file_name = uniqid() . '.' . $file->getClientOriginalExtension();

        $imgFile = Image::make($file->getRealPath());

        $imgFile->resize(800, 800, function ($constraint) {
		    $constraint->aspectRatio();
		})->encode('jpg', 80);

        
        Storage::disk('public')->putFileAs($company_id.'/cattle',
            $file,
            $file_name
        );
        
        return '/uploads/'.$company_id.'/cattle/'.$file_name;
    }
    
    public function data(Request $request)
    {
        $sort = !empty($request->sort) ? $request->sort : 'id';
        $sortDir = !empty($request->sortDir) ? $request->sortDir : 'desc';
        $limit = ($request->limit) ? $request->limit : 25;

        $company_id = auth()->user()->company_id;

        $query = DB::table("medical_check as mc")
        ->join('res_cattle as rc', 'rc.id', '=', 'mc.cattle_id')
        ->join('res_type as rt', 'rt.id', '=', 'rc.type_id')
        ->join('location as l', 'l.id', '=', 'rc.location_id')
        ->select("mc.id", "mc.date", "mc.weight", "mc.status", "mc.description", "mc.image", "rc.eartag", "rt.name as type", "l.name as location", "rc.jk")
        ->where('mc.company_id', $company_id);
        
        if($request->page){
            $data = $query->paginate($limit);
        }else{
            $data = $query->get();
        }
        
        return response()->json($data);
    }
}
