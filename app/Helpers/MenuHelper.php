<?php

namespace App\Helpers;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use DB;

class MenuHelper
{
    public static function get(){

        $menuData = Collect([]);

        
        $menuData->push([
            "icon" => "fi fi-rs-home",
            "name" => "Dashboard",
            "to" => "user.dashboard",
        ]);

        $menuData->push([
            "name" => 'Pembelian',
            "icon" => 'fi fi-rs-users',
            "subActivePaths" => 'user.dukungan.*',
            "sub" => [
                [
                    "name" => 'Buat Pembelian',
                    "to" => 'user.dukungan.relawan',
                ],
                [
                    "name" => 'List Pembelian',
                    "to" => 'user.dukungan.pendukung',
                ],
                [
                    "name" => 'Supplier',
                    "to" => 'user.dukungan.pendukung',
                ]
            ]
        ]);
        
        return $menuData->all();
    }

    public static function permission()
    {
        $data = auth()->guard('admin')->user()->getAllPermissions()->toArray();
        $permission = array();
        foreach ($data as $element) {
            $permission[$element['module']][] = $element['name'];
        }

        return $permission;
    }
}
