<?php

namespace App\Helpers;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use DB;

class SquenceHelper
{
    public static function currency($value){
        return number_format($value,0,',','.');
    }

    
    public static function seq_purchase($company_id)
    {
        $q = DB::table('purchase')->select(DB::raw('MAX(RIGHT(name,5)) AS kd_max'))->where('company_id', $company_id);

        $code = 'BILL/';
        $no = 1;
        date_default_timezone_set('Asia/Jakarta');

        if($q->count() > 0){
            foreach($q->get() as $k){
                return $code . date('ym') .'/'.sprintf("%05s", abs(((int)$k->kd_max) + 1));
            }
        }else{
            return $code . date('ym') .'/'. sprintf("%05s", $no);
        }
    }

    public static function seq_sale($company_id)
    {
        $q = DB::table('sale')->select(DB::raw('MAX(RIGHT(name,5)) AS kd_max'))->where('company_id', $company_id);
        $urut = "";

        $code = 'SO/';
        $no = 1;
        date_default_timezone_set('Asia/Jakarta');

        if($q->count() > 0){
            foreach($q->get() as $k){
                return $code . date('ym') .'/'.sprintf("%05s", abs(((int)$k->kd_max) + 1));
            }
        }else{
            return $code . date('ym') .'/'. sprintf("%05s", $no);
        }
    }


    public static function seq_stock_in($company_id)
    {
        $q = DB::table('stock_move')->select(DB::raw('MAX(RIGHT(name,5)) AS kd_max'))->where('company_id', $company_id);
        $urut = "";

        $code = 'WH/IN/';
        $no = 1;
        date_default_timezone_set('Asia/Jakarta');

        if($q->count() > 0){
            foreach($q->get() as $k){
                return $code . date('ym') .'/'.sprintf("%05s", abs(((int)$k->kd_max) + 1));
            }
        }else{
            return $code . date('ym') .'/'. sprintf("%05s", $no);
        }
    }

    
    public static function seq_stock_out($company_id)
    {
        $q = DB::table('stock_move')->select(DB::raw('MAX(RIGHT(name,5)) AS kd_max'))->where('company_id', $company_id);
        $urut = "";

        $code = 'WH/OUT/';
        $no = 1;
        date_default_timezone_set('Asia/Jakarta');

        if($q->count() > 0){
            foreach($q->get() as $k){
                return $code . date('ym') .'/'.sprintf("%05s", abs(((int)$k->kd_max) + 1));
            }
        }else{
            return $code . date('ym') .'/'. sprintf("%05s", $no);
        }
    }

    
    public static function seq_invest($company_id)
    {
        $q = DB::table('account_invest')->select(DB::raw('MAX(RIGHT(name,5)) AS kd_max'))->where('company_id', $company_id);
        $urut = "";

        $code = 'INV/';
        $no = 1;
        date_default_timezone_set('Asia/Jakarta');

        if($q->count() > 0){
            foreach($q->get() as $k){
                return $code . date('ym') .'/'.sprintf("%05s", abs(((int)$k->kd_max) + 1));
            }
        }else{
            return $code . date('ym') .'/'. sprintf("%05s", $no);
        }
    }

    
    public static function seq_cash($company_id, $type)
    {
        $q = DB::table('account_cash')->select(DB::raw('MAX(RIGHT(name,5)) AS kd_max'))
        ->where('type', $type)
        ->where('company_id', $company_id);
        $code = $type == 'income' ? 'CSH/IN/' : 'CSH/OUT/';

        $no = 1;
        date_default_timezone_set('Asia/Jakarta');
        if($q->count() > 0){
            foreach($q->get() as $k){
                return $code . date('ym') .'/'.sprintf("%05s", abs(((int)$k->kd_max) + 1));
            }
        }else{
            return $code . date('ym') .'/'. sprintf("%05s", $no);
        }
    }

}