<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LocationPent extends Model
{
    protected $table = 'location_pents';
    protected $primaryKey = 'id';

    protected $fillable = [
        'id', 'nama', 'code', 'capacity'
    ];
    
}
