<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $table = 'location';
    protected $primaryKey = 'id';

    protected $fillable = [
        'id', 'nama', 'code', 'address'
    ];

    public function lines()
    {
        return $this->hasMany(LocationPent::class, 'location_id');
    }
    
}
