<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    protected $table = 'purchase';
    protected $primaryKey = 'id';

    protected $fillable = [
        'id', 'name', 'company_id', 'supplier_id', 'order_date', 'due_date', 'total', 'discount_type', 'discount_value', 'grand_total'
    ];

    
    public function line()
    {
        return $this->hasMany(PurchaseLine::class, 'order_id');
    }

    
    public function move()
    {
        return $this->morphMany(StockMove::class, 'ordertable');
    }

    
    public function payment()
    {
        return $this->morphMany(Payment::class, 'paymenttable');
    }
    
}
