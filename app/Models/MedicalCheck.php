<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MedicalCheck extends Model
{
    protected $table = 'medical_check';
    protected $primaryKey = 'id';

    protected $fillable = [
        'id', 'nama', 'code', 'capacity'
    ];
    
}
