<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SaleLine extends Model
{
    protected $table = 'sale_line';
    protected $primaryKey = 'id';

    protected $fillable = [
        'id', 'order_id', 'company_id', 'eartag', 'type_id', 'weight', 'price'
    ];

    
    public function sale(){
        return $this->belongsTo(Sale::class, 'order_id');
    }
    
}
