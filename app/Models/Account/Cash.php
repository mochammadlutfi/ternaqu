<?php

namespace App\Models\Account;

use Illuminate\Database\Eloquent\Model;

class Cash extends Model
{
    protected $table = 'account_cash';
    protected $primaryKey = 'id';

    protected $fillable = [
        'id', 'journal_id', 'type', 'debit','credit', 'saldo' 
    ];
    
}
