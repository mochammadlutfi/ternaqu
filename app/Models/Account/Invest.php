<?php

namespace App\Models\Account;

use Illuminate\Database\Eloquent\Model;

class Invest extends Model
{
    protected $table = 'account_invest';
    protected $primaryKey = 'id';

    protected $fillable = [
        'id', 'investor_id', 'amount', 'journal_id'
    ];
    
}
