<?php

namespace App\Models\Account;

use Illuminate\Database\Eloquent\Model;

class Journal extends Model
{
    protected $table = 'account_journal';
    protected $primaryKey = 'id';

    protected $fillable = [
        'id', 'type', 'code', 'name', 'bank_id'
    ];
    
}
