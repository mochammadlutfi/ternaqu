<?php

namespace App\Models\Account;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $table = 'account_bank';
    protected $primaryKey = 'id';

    protected $fillable = [
        'id', 'name', 'number'
    ];
    
}
