<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PurchaseLine extends Model
{
    protected $table = 'purchase_line';
    protected $primaryKey = 'id';

    protected $fillable = [
        'id', 'order_id', 'company_id', 'eartag', 'type_id', 'weight', 'price'
    ];

    
    public function purchase(){
        return $this->belongsTo(Purchase::class, 'order_id');
    }
    
}
