<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockMove extends Model
{
    protected $table = 'stock_move';
    protected $primaryKey = 'id';

    protected $fillable = [
        'id', 'name', 'company_id', 'supplier_id', 'order_date', 'due_date', 'total', 'discount_type', 'discount_value', 'grand_total'
    ];

    
    public function line()
    {
        return $this->hasMany(StockMoveLine::class, 'move_id');
    }
    
}
