<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockMoveLine extends Model
{
    protected $table = 'stock_move_line';
    protected $primaryKey = 'id';

    protected $fillable = [
        'id', 'order_id', 'company_id', 'eartag', 'type_id', 'weight', 'price'
    ];

    
    public function cattle(){
        return $this->belongsTo(Cattle::class, 'cattle_id');
    }

    public function purchase(){
        return $this->belongsTo(Purchase::class, 'order_id');
    }
    
}
