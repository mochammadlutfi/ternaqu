<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $table = 'res_vehicle';
    protected $primaryKey = 'id';

    protected $fillable = [
        'id', 'nama', 'code', 'address'
    ];
    
}
