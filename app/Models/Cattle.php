<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cattle extends Model
{
    protected $table = 'res_cattle';
    protected $primaryKey = 'id';

    protected $fillable = [
        'id', 'nama', 'code', 'address'
    ];
    
}
