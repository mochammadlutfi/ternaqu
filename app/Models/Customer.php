<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'res_partner';
    protected $primaryKey = 'id';

    protected $fillable = [
        'id', 'nama', 'level', 'daerah_id'
    ];

    protected $casts = [
        'daerah_id' => 'string'
    ];
    
}
