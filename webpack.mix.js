const mix = require('laravel-mix');
const path = require('path');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .vue()
    .sass('resources/sass/styles.scss', 'public/css')
    .webpackConfig({
        output: { chunkFilename: 'js/[name].[contenthash].js' },
        plugins: [

        ],
        resolve: {
            alias: {
                '@': path.resolve('resources/js'),
                ziggy: path.resolve('vendor/tightenco/ziggy/dist/vue'),
            },
        },
      })
      .options({
          processCssUrls: false
      });

if (mix.inProduction()) {
    mix.version();
}
