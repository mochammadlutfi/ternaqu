require('./bootstrap');

import { createApp, h } from 'vue';
import { createInertiaApp } from '@inertiajs/inertia-vue3';
import { InertiaProgress } from '@inertiajs/progress';
import BootstrapVueNext from 'bootstrap-vue-next'
import moment from 'moment';
import BaseLayout from "@/Layouts/BaseLayout";
import VueTippy from 'vue-tippy';
import VueSweetalert2 from 'vue-sweetalert2';
import Vue3MobileDetection from "vue3-mobile-detection";

moment.locale('id');

const appName = window.document.getElementsByTagName('title')[0]?.innerText || 'Laravel';
const cleanApp = () => {
    document.getElementById('app').removeAttribute('data-page')
}
createInertiaApp({
    title: (title) => `${title} - ${appName}`,
    resolve: (name) => require(`./Pages/${name}.vue`),
    setup({ el, app, props, plugin }) {
        return createApp({ render: () => h(app, props) })
            .use(plugin)
            .use(moment)
            .use(BootstrapVueNext)
            .use(Vue3MobileDetection)
            .use(VueTippy, {
                defaultProps: { placement: 'right' },
            })
            .use(VueSweetalert2,{
                confirmButtonColor: '#0284c7',
                cancelButtonColor: '#dc2626',
            })
            .component("BaseLayout", BaseLayout)
            .mixin({
                methods: { 
                    route,
                    currency(value){
                        if (typeof value !== "number") {
                            // return value;
                            value = parseFloat(value);
                        }
                        var formatter = new Intl.NumberFormat('id-ID', {
                            style: 'currency',
                            currency: 'IDR',
                            minimumFractionDigits: 0
                        });
                        return formatter.format(value);
                    },
                } 
            })
            .mount(el);
    },
}).then(cleanApp);

InertiaProgress.init({ color: '#4B5563' });
