import { createVuetify } from 'vuetify'
import { VBtn } from 'vuetify/components'
import defaults from './defaults'
import { icons } from './icons'
import theme from './theme'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'

// Styles
import 'vuetify/styles'

export default createVuetify({
    components,
    directives,
        aliases: {
            IconBtn: VBtn,
        },
    defaults,
    icons,
    theme,
})
