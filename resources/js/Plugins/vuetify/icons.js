// import { aliases, mdi } from 'vuetify/iconsets/mdi-svg'
import '@mdi/font/css/materialdesignicons.css'
import { aliases, fa } from 'vuetify/iconsets/fa'
import { mdi } from 'vuetify/iconsets/mdi-svg'

export const icons = {
    defaultSet: 'mdi',
    aliases,
    sets: {
        fa,
        mdi,
    },
}