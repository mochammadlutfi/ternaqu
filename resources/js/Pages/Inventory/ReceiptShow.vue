<template>
    <base-layout title="Detail Penerimaan" hideFooter>
        <div class="content">
            <div class="content-heading d-flex justify-content-between align-items-center"  v-if="!$isMobile()">
                <span>
                    Detail Penerimaan
                </span>
                <div class="space-x-1">
                    <template v-if="!data.state">
                        <button type="button" class="btn btn-sm btn-success" @click.prevent="update()">
                            <i class="fa fa-fw fa-check me-1"></i>
                            <span class="d-none d-sm-inline">Terima</span>
                        </button>
                    </template>
                </div>
            </div>
            <div class="block block-rounded block-fx-shadow">
                <div class="block-content p-3">
                    <h2>{{ data.name }}</h2>
                    <div class="row mb-4">
                        <div class="col-md-6">
                            <div class="row mb-2">
                                <div class="col-5 col-md-4 d-flex">
                                    <label for="input-ref" class="my-auto fw-semibold">Supplier</label>
                                </div>
                                <div class="col-7 col-md-8 text-end text-md-start">
                                    {{  data.partner }}
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-5 col-md-4 d-flex">
                                    <label for="input-ref" class="my-auto fw-semibold">No Pembelian</label>
                                </div>
                                <div class="col-7 col-md-8 text-end text-md-start">
                                    {{  data.source }}
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-5 col-md-4 d-flex">
                                    <label for="input-ref" class="my-auto fw-semibold">No Surat Jalan</label>
                                </div>
                                <div class="col-7 col-md-8 text-end text-md-start">
                                    <template v-if="data.state">
                                        {{ data.ref }}
                                    </template>
                                    <template v-else>
                                        <input type="text" class="form-control form-control-sm" :class="{'is-invalid' : errors.ref}" id="input-ref" v-model="form.ref" >
                                        <div id="input-ref" class="invalid-feedback"></div>
                                    </template>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row mb-2">
                                <div class="col-5 col-md-4">
                                    <label for="input-ref" class="my-auto fw-semibold">Jadwal Penerimaan</label>
                                </div>
                                <div class="col-7 col-md-8 text-end text-md-start">
                                    {{ date(data.schedule_date) }}
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-5 col-md-4 d-flex">
                                    <label for="input-ref" class="my-auto fw-semibold">Lokasi Penerimaan</label>
                                </div>
                                <div class="col-7 col-md-8 text-end text-md-start">
                                    {{  data.location }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <template v-if="$isMobile()">
                        <div class="block block-bordered rounded-2 mb-2" v-for="(line, index) in data.lines" :key="index">
                            <div class="block-content d-flex justify-content-between p-2">
                                <div class="text-left">
                                    <div class="fw-semibold fs-6">{{ line.type }}</div>
                                    <div class="fs-sm">
                                        {{ line.jk }}
                                    </div>
                                    <div class="fs-sm">
                                        {{ date(line.dob)}}
                                    </div>
                                    <div class="fs-sm">
                                        {{ line.weight}} Kg
                                    </div>
                                </div>
                                <div class="my-auto">
                                    <div class="img-upload" v-if="data.state">
                                        <div class="img-upload_wrapper">
                                            <img class="img-fluid" :src="line.image">
                                        </div>
                                    </div>
                                    <image-upload @change="e => lines[index].image = e" v-else/>
                                </div>
                            </div>
                            <div class="block-content border-2 border-top d-flex justify-content-between p-2" v-if="line.investor">
                                <div class="fw-semibold fs-6">{{ line.investor }}</div>
                                <div class="fw-semibold fs-6">{{ date2(line.mou_date) }}</div>
                            </div>
                            <div class="block-content border-top border-2 p-2">
                                <div class="row mb-2">
                                    <div class="col-5 d-flex">
                                        <label for="input-ref" class="my-auto fw-semibold">Eartag</label>
                                    </div>
                                    <div class="col-7 text-end">
                                        <template v-if="data.state">{{ lines[index].eartag }}</template>
                                        <template v-else>
                                            <input type="text" class="form-control" 
                                            :value="lines[index].eartag" 
                                            @input="event => lines[index].eartag = event.target.value"
                                            :class="{'is-invalid' : v$.lines.$each.$response.$errors[index].eartag.length }" />
                                            <div class="invalid-feedback" v-if="v$.lines.$each.$response.$errors[index].eartag.length">
                                                {{ v$.lines.$each.$response.$errors[index].eartag[0].$message }}
                                            </div>
                                        </template>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </template>
                    <table class="table table-bordered table-vcenter" v-else>
                        <thead class="bg-body">
                            <tr>
                                <th width="20%">Request Ternak</th>
                                <th width="20%">Investor</th>
                                <th width="17%">Eartag</th>
                                <th width="8%">Berat</th>
                                <th width="5%">Status</th>
                                <th width="10%">Foto</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(line, index) in data.lines" :key="index">
                                <td>
                                    <div class="fs-6 fw-bold">{{ line.type }}</div>
                                    <p class="text-muted mb-0">
                                        <i class="fa fa-venus-mars"></i> {{ line.jk }} <br>
                                        <i class="fa fa-weight-scale"></i> {{ line.weight }} Kg
                                    </p>
                                </td>
                                <td>
                                    <template v-if="line.investor_id">
                                        <div class="fw-bold fs-6">{{ line.investor }}</div>
                                        <div class="fs-sm">{{ date2(line.mou_date) }}
                                        </div>
                                    </template>
                                   
                                </td>
                                <td>
                                    <template v-if="data.state">{{ lines[index].eartag }}</template>
                                    <template v-else>
                                        <input type="text" class="form-control" 
                                        :value="lines[index].eartag" 
                                        @input="event => lines[index].eartag = event.target.value"
                                        :class="{'is-invalid' : v$.lines.$each.$response.$errors[index].eartag.length }" />
                                        <div class="invalid-feedback" v-if="v$.lines.$each.$response.$errors[index].eartag.length">
                                            {{ v$.lines.$each.$response.$errors[index].eartag[0].$message }}
                                        </div>
                                    </template>
                                </td>
                                <td>
                                    {{ line.weight }} Kg
                                </td>
                                <td>
                                    <b-form-checkbox :checked="lines[index].state" name="check-button" @change="e => lines[index].state = e" switch :disabled="data.state == 1 ? true : false"></b-form-checkbox>
                                </td>
                                <td>
                                    <div class="img-upload" v-if="data.state">
                                        <div class="img-upload_wrapper">
                                            <img class="img-fluid" :src="line.image">
                                        </div>
                                    </div>
                                    <image-upload @change="e => lines[index].image = e" v-else/>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="footer-nav-area"  v-if="$isMobile() && !data.state">
            <div class="content">
                <button type="button" class="btn btn-sm btn-success w-100" @click.prevent="update">
                    <i class="fa fa-fw fa-check me-1"></i>
                   Konfirmasi
                </button>
            </div>
        </div>
    </base-layout>
</template>
<script>
import ImageUpload from '@/Components/Form/ImageUpload';
import moment from 'moment';
import flatPickr from 'vue-flatpickr-component';
import { useVuelidate } from '@vuelidate/core'
import { required,helpers  } from '@vuelidate/validators'
export default {
    setup () {
        return {
        v$: useVuelidate()
        }
    },
    props :{
        data : Object,
        errors : Object
    },
    components : {
        flatPickr,
        ImageUpload
    },
    data(){
        return {
            loading : false,
            form : {
                date_done : new Date(),
                location_id : null,
                ref : null,
            },
            lines : [],
            dateConfig: {
                altFormat: 'j F Y H:i',
                altInput: true,
                dateFormat: 'Y-m-d H:i',
                showMonths: 1,
                enableTime: true,
                time_24hr: true
            },
        }
    },
    created(){
        if(this.data.lines.length > 0){
            this.lines = [];
            this.data.lines.forEach((value, index) => {
                let line = {
                    id : value.id,
                    ref : value.ref,
                    eartag : value.eartag,
                    state : value.state,
                    image : value.image,
                }
                this.lines.push(line);
            });
        }
    },
    methods : {
        updateImage(e){
            console.log(e);
        },
        date(value){
            if (value) {
                return moment(String(value)).format('DD MMMM YYYY hh:mm')
            }
        },
        date2(value){
            if (value) {
                return moment(String(value)).format('MMMM YYYY')
            }
        },
        async update(){
            this.loading = true;
            this.v$.lines.$validate();
            if (this.v$.lines.$error) {
                this.loading = false;
                return
            }
            let data = Object.assign(this.form, {
                    lines : this.lines,
                }
            );
            let form = this.$inertia.form(data)
            let url = this.route("user.inventory.update", {state : 'in', id : this.data.id });
            form.post(url, {
                preserveScroll: true,
                resetOnSuccess: true,
                onSuccess: () => {
                    this.$bvToast.toast(this.$page.props.flash.message, {
                        autoHideDelay: 15000,
                    });
                },
                onFinish: () => {
                    this.loading = false;
                },
            });
        }
    },
    validations () {
        return {
            lines : {
                $each: helpers.forEach({
                    eartag : {
                        required : helpers.withMessage('Eartag Wajib Diisi!', required),
                    }
                })
            }
        }
    },
}
</script>